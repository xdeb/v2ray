#!/bin/bash

test -f ./make_test_data.env
. ./make_test_data.env
PS4='\n\[\033[1;92m\]$(date +%FT%T%z) ${HOSTNAME}:${0}:${LINENO} + \e[m\]'

set -eux

srcdir=$CI_PROJECT_DIR/v2ray-core-$PKG_VERSION
pwd=$(pwd)
tarball_name=v2ray-core-$PKG_VERSION.tar.gz


##############################
##                          ##
##  Caching `nfpm` command  ##
##                          ##
##############################

test -x /tmp/nfpm || wget --no-verbose -O- https://xdeb.gitlab.io/nfpm/nfpm-static_amd64.tar.gz | tar -C /tmp -xz


#################################
##                             ##
##  CI JOB: cache-source-code  ##
##                             ##
#################################

# Local cached source code tarball file can speed up process
test -e /tmp/$tarball_name || wget -O /tmp/$tarball_name https://github.com/v2fly/v2ray-core/archive/refs/tags/v$PKG_VERSION.tar.gz


#################################
##                             ##
##                             ##
#################################

test -e /tmp/geoip.dat || wget -O /tmp/geoip.dat https://github.com/v2fly/geoip/raw/release/geoip.dat
test -e /tmp/geoip-only-cn-private.dat || wget -O /tmp/geoip-only-cn-private.dat https://github.com/v2fly/geoip/raw/release/geoip-only-cn-private.dat
test -e /tmp/geosite.dat || wget -O /tmp/geosite.dat https://github.com/v2fly/domain-list-community/raw/release/dlc.dat


##############
##          ##
##  BUILDS  ##
##          ##
##############

function build_in_container() {
    local os ctname args vol_cache_gobuild vol_cache_pkgmod

    os=$1
    test -n "$os"

    ctname="$PKG_NAME-$PKG_VERSION-builder-$os"
    vol_cache_gobuild=cache-$PKG_NAME-$PKG_VERSION-$os-gobuild
    vol_cache_pkgmod=cache-$PKG_NAME-$PKG_VERSION-$os-pkgmod

    args=""
    case $os in
        bullseye|bookworm)
            args="$args -e DEBIAN_FRONTEND=noninteractive"
            ;;
        alpine) ;;
        *)
            echo "ERROR: os not found: $os"
            exit 1
    esac

    test -e /tmp/$tarball_name
    test -x /tmp/nfpm
    test -e /tmp/geoip.dat
    test -e /tmp/geoip-only-cn-private.dat
    test -e /tmp/geosite.dat

    # Set up builder container
    docker rm -f $ctname
    docker run --name=$ctname -d -t \
        -v $vol_cache_gobuild:/root/.cache/go-build \
        -v $vol_cache_pkgmod:/go/pkg/mod \
        -v /tmp/$tarball_name:$CI_PROJECT_DIR/$tarball_name:ro \
        -v /tmp/nfpm:/usr/local/bin/nfpm:ro \
        -v /tmp/geoip.dat:$CI_PROJECT_DIR/dist/noarch/geoip.dat:ro \
        -v /tmp/geoip-only-cn-private.dat:$CI_PROJECT_DIR/dist/noarch/geoip-only-cn-private.dat:ro \
        -v /tmp/geosite.dat:$CI_PROJECT_DIR/dist/noarch/geosite.dat:ro \
        -w $CI_PROJECT_DIR \
        -h $ctname \
        --env-file ./make_test_data.env \
        $args \
        golang:$TARGET_GOLANG_VERSION-$os
    docker exec -it $ctname tar -xf $tarball_name

    # Use local comstomized goproxy settings to speed up downloading
    test -n "$(go env GOPROXY)" && docker exec -it $ctname go env -w GOPROXY="$(go env GOPROXY)"
    set +u
    test -n "$GOPROXY" &&
        # $GOPROXY is higher than $(go env GOPROXY) in priority
        docker exec -it $ctname go env -w GOPROXY="$GOPROXY"
    set -u

    # Simulate `git clone`: Copy all source files from the repository into the container.
    sh -c "for f in *; do docker cp \$f $ctname:$CI_PROJECT_DIR/\$f; done"

    # patching nfpm.yaml and static-nfpm.yaml to avoid signature
    docker exec -it $ctname sed -i '/^# import the sign key$/{N;N;d;}' $CI_PROJECT_DIR/ci-build.sh
    docker exec -it $ctname sed -i '/^  signature:$/{N;N;d;}' $CI_PROJECT_DIR/nfpm.yaml
    docker exec -it $ctname sed -i '/^  signature:$/{N;N;d;}' $CI_PROJECT_DIR/nfpm-static.yaml

    if [ "$os" = "alpine" ]; then
        docker exec -it $ctname sed -i s/https:/http:/ /etc/apk/repositories
        docker exec -it $ctname apk --no-cache add bash

        # Do build
        docker exec -it $ctname ./ci-build.sh

        # Gain artifacts
        docker exec -it $ctname bash -c "tar -czf artifacts.tar.gz dist/$os/{386,amd64}/$PKG_NAME dist/*.apk dist/*.deb"
        docker cp $ctname:$CI_PROJECT_DIR/artifacts.tar.gz /tmp/${ctname}_artifacts.tar.gz
    else
        # Use local comstomized apt.conf to speed up `apt-get install`, if posible
        test -e /etc/apt/apt.conf && docker cp /etc/apt/apt.conf $ctname:/etc/apt/apt.conf

        # Do build
        docker exec -it $ctname ./ci-build.sh

        # Gain artifacts
        docker exec -it $ctname bash -c "tar -czf artifacts.tar.gz dist/$os/{386,amd64}/$PKG_NAME dist/*.deb"
        docker cp $ctname:$CI_PROJECT_DIR/artifacts.tar.gz /tmp/${ctname}_artifacts.tar.gz
    fi
}

##  CI JOB: build-bullseye-based-dynamic
build_in_container bullseye

##  CI JOB: build-bookworm-based-dynamic
build_in_container bookworm

##  CI JOB: build-static
build_in_container alpine


#############
##         ##
##  TESTS  ##
##         ##
#############

function test_debian() {
    local codename=$1
    local args=""

    test -e /etc/apt/apt.conf && args="-v /etc/apt/apt.conf:/etc/apt/apt.conf:ro"

    case $codename in
        buster|bullseye|bookworm) os=debian ;;
        focal|jammy|lunar|mantic) os=ubuntu ;;
    esac

    test -f /tmp/$PKG_NAME-$PKG_VERSION-builder-alpine_artifacts.tar.gz
    test -f /tmp/$PKG_NAME-$PKG_VERSION-builder-bullseye_artifacts.tar.gz
    test -f /tmp/$PKG_NAME-$PKG_VERSION-builder-bookworm_artifacts.tar.gz
    test -x $pwd/ci-test.sh
    test -d $pwd/test_data

    docker run --rm -it \
        -v $pwd/ci-test.sh:/bin/ci-test.sh:ro \
        -v /tmp/$PKG_NAME-$PKG_VERSION-builder-alpine_artifacts.tar.gz:/tmp/alpine_artifacts.tar.gz:ro \
        -v /tmp/$PKG_NAME-$PKG_VERSION-builder-bullseye_artifacts.tar.gz:/tmp/bullseye_artifacts.tar.gz:ro \
        -v /tmp/$PKG_NAME-$PKG_VERSION-builder-bookworm_artifacts.tar.gz:/tmp/bookworm_artifacts.tar.gz:ro \
        -v $pwd/test_data:/test_data:ro \
        -h test-$codename \
        --env-file ./make_test_data.env \
        -e CODENAME=$codename \
        -e DEBIAN_FRONTEND=noninteractive \
        $args \
        $os:$codename sh -c "set -eux
        tar -xf /tmp/alpine_artifacts.tar.gz
        tar -xf /tmp/bullseye_artifacts.tar.gz
        tar -xf /tmp/bookworm_artifacts.tar.gz
        apt-get update
        apt-get install tree
        tree /dist/
        ci-test.sh"
}

function test_alpine() {
    test -f /tmp/$PKG_NAME-$PKG_VERSION-builder-alpine_artifacts.tar.gz
    test -x $pwd/ci-test-static.sh
    test -d $pwd/test_data

    docker run --rm -it \
        -v $pwd/ci-test-static.sh:/bin/ci-test-static.sh:ro \
        -v /tmp/$PKG_NAME-$PKG_VERSION-builder-alpine_artifacts.tar.gz:/tmp/alpine_artifacts.tar.gz:ro \
        -v $pwd/test_data:/test_data:ro \
        -h test-alpine \
        --env-file ./make_test_data.env \
        -e CODENAME=alpine \
        alpine:latest sh -c "set -eux
        tar -xf /tmp/alpine_artifacts.tar.gz
        sed -i s/https:/http:/ /etc/apk/repositories
        apk add bash tree
        tree /dist/
        ci-test-static.sh"
}

test_debian buster
test_debian bullseye
test_debian bookworm
test_debian focal
test_debian jammy
test_debian lunar
test_debian mantic
test_alpine


################################
##                            ##
##  COPY OUT .DEB, .APK FILE  ##
##                            ##
################################

mkdir -p test_data
rm -fv test_data/*.deb test_data/*.apk

tmpdir=$(mktemp -d)
tar -C $tmpdir -xf /tmp/$PKG_NAME-$PKG_VERSION-builder-bullseye_artifacts.tar.gz
tar -C $tmpdir -xf /tmp/$PKG_NAME-$PKG_VERSION-builder-bookworm_artifacts.tar.gz
tar -C $tmpdir -xf /tmp/$PKG_NAME-$PKG_VERSION-builder-alpine_artifacts.tar.gz
cp -v $tmpdir/dist/${PKG_NAME}_${PKG_VERSION}-${PKG_RELEASE}-xdeb~buster_amd64.deb \
    $tmpdir/dist/${PKG_NAME}_${PKG_VERSION}-${PKG_RELEASE}-xdeb~bullseye_amd64.deb \
    $tmpdir/dist/${PKG_NAME}_${PKG_VERSION}-${PKG_RELEASE}-xdeb~focal_amd64.deb \
    $tmpdir/dist/${PKG_NAME}_${PKG_VERSION}-${PKG_RELEASE}-xdeb~bookworm_amd64.deb \
    $tmpdir/dist/${PKG_NAME}_${PKG_VERSION}-${PKG_RELEASE}-xdeb~jammy_amd64.deb \
    $tmpdir/dist/${PKG_NAME}_${PKG_VERSION}-${PKG_RELEASE}-xdeb~lunar_amd64.deb \
    $tmpdir/dist/${PKG_NAME}_${PKG_VERSION}-${PKG_RELEASE}-xdeb~mantic_amd64.deb \
    $tmpdir/dist/${PKG_NAME}-static_${PKG_VERSION}-${PKG_RELEASE}-xdeb_amd64.deb \
    test_data/
rm -rf $tmpdir
