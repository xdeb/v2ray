FROM debian:bookworm AS busybox
ENV DEBIAN_FRONTEND=noninteractive
RUN set -eux ;\
    apt-get update ;\
    apt-get install --yes busybox

FROM debian:bookworm
ADD ./dist/bookworm/amd64/v2ray \
    ./dist/noarch/geoip.dat \
    ./dist/noarch/geoip-only-cn-private.dat \
    ./dist/noarch/geosite.dat \
    /usr/lib/v2ray/
COPY --from=busybox /bin/busybox /bin/busybox
RUN set -eux ;\
    ln -sv /usr/lib/v2ray/v2ray /usr/bin/v2ray ;\
    mkdir /etc/v2ray ;\
    ln -sv /bin/busybox /bin/ping ;\
    ln -sv /bin/busybox /sbin/ifconfig ;\
    ln -sv /bin/busybox /sbin/ip ;\
    ln -sv /bin/busybox /sbin/ipaddr ;\
    ln -sv /bin/busybox /sbin/route ;\
    ln -sv /bin/busybox /usr/bin/traceroute ;\
    ln -sv /bin/busybox /usr/bin/vi ;\
    ln -sv /bin/busybox /usr/bin/wget
ENTRYPOINT ["/usr/bin/v2ray", "-c", "/etc/v2ray/config.json"]

# vim: ts=4 sw=4 et
