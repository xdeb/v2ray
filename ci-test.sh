#!/bin/bash

if [ "$HOSTNAME" = "${HOSTNAME#runner-}" ] # Don't print HOSTNAME on GitLab Runner.
then
    PS4='\n\[\033[1;93m\]$(date +%FT%T%z) ${HOSTNAME}:${0}:${LINENO} + \e[m\]'
else
    PS4='\n\[\033[1;93m\]$(date +%FT%T%z) ${0}:${LINENO} + \e[m\]'
fi

. /etc/os-release

set -eux


##########################
##                      ##
##  SET UP TESTING ENV  ##
##                      ##
##########################

case $ID in
debian|ubuntu)
    codename=$VERSION_CODENAME
    ;;
*)
    echo "ERROR: case not found: ID='$ID'"
    exit 1
esac


################
##            ##
##  DO TESTS  ##
##            ##
################

# dynamic linked binary
case $codename in
buster|bullseye|focal)
    ./dist/bullseye/amd64/v2ray version | head -n1 | grep "^V2Ray $PKG_VERSION (V2Fly) " ;;
bookworm|jammy|lunar|mantic)
    ./dist/bookworm/amd64/v2ray version | head -n1 | grep "^V2Ray $PKG_VERSION (V2Fly) " ;;
*)
    echo "ERROR:\ codename not found:\ $codename"
    exit 1 ;;
esac
./dist/bullseye/386/v2ray version | head -n1 | grep "^V2Ray $PKG_VERSION (V2Fly) "
./dist/bookworm/386/v2ray version | head -n1 | grep "^V2Ray $PKG_VERSION (V2Fly) "

# static linked binary
./dist/alpine/amd64/v2ray version | head -n1 | grep "^V2Ray $PKG_VERSION (V2Fly) "
./dist/alpine/386/v2ray version | head -n1 | grep "^V2Ray $PKG_VERSION (V2Fly) "

# install .deb package (dynamic)
apt-get install --no-install-recommends -y ./dist/v2ray_${PKG_VERSION}-${PKG_RELEASE}-xdeb~${codename}_amd64.deb
/usr/bin/v2ray version | head -n1 | grep "^V2Ray $PKG_VERSION (V2Fly) "

# uninstall .deb package (dynamic)
test -x /usr/bin/v2ray
apt-get autoremove --purge --yes v2ray
test ! -x /usr/bin/v2ray

# upgrade .deb package (dynamic)
apt-get install --no-install-recommends -y ./test_data/v2ray_5.4.0-1-xdeb~${codename}_amd64.deb
/usr/bin/v2ray version | head -n1 | grep "^V2Ray 5.4.0 (V2Fly) "
apt-get install --no-install-recommends -y ./dist/v2ray_${PKG_VERSION}-${PKG_RELEASE}-xdeb~${codename}_amd64.deb
/usr/bin/v2ray version | head -n1 | grep "^V2Ray $PKG_VERSION (V2Fly) "

# install .deb package (static)
apt-get install --no-install-recommends -y ./dist/v2ray-static_${PKG_VERSION}-${PKG_RELEASE}-xdeb_amd64.deb
/usr/bin/v2ray version | head -n1 | grep "^V2Ray $PKG_VERSION (V2Fly) "

# uninstall .deb package (static)
test -x /usr/bin/v2ray
apt-get autoremove --purge --yes v2ray-static
test ! -x /usr/bin/v2ray

# upgrade .deb package (static)
apt-get install --no-install-recommends -y ./test_data/v2ray-static_5.4.0-1-xdeb_amd64.deb
/usr/bin/v2ray version | head -n1 | grep "^V2Ray 5.4.0 (V2Fly) "
apt-get install --no-install-recommends -y ./dist/v2ray-static_${PKG_VERSION}-${PKG_RELEASE}-xdeb_amd64.deb
/usr/bin/v2ray version | head -n1 | grep "^V2Ray $PKG_VERSION (V2Fly) "
