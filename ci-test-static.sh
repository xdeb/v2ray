#!/bin/bash

if [ "$HOSTNAME" = "${HOSTNAME#runner-}" ] # Don't print HOSTNAME on GitLab Runner.
then
    PS4='\n\[\033[1;93m\]$(date +%FT%T%z) ${HOSTNAME}:${0}:${LINENO} + \e[m\]'
else
    PS4='\n\[\033[1;93m\]$(date +%FT%T%z) ${0}:${LINENO} + \e[m\]'
fi

set -eux


################
##            ##
##  DO TESTS  ##
##            ##
################

# static linked binary
apk --no-cache add go
./dist/alpine/amd64/v2ray version | head -n1 | grep "^V2Ray $PKG_VERSION (V2Fly) "
./dist/alpine/386/v2ray version | head -n1 | grep "^V2Ray $PKG_VERSION (V2Fly) "
apk del go

# Note: the `nfpm` which is the deb package maker uses 2 types of .apk filename format.
# nfpm <= v2.33.1 generate .apk file in format *-${PKG_VERSION}-${PKG_RELEASE}-xdeb_*.apk
# nfpm >= v2.34.0 generate .apk file in format *-${PKG_VERSION}_p${PKG_RELEASE}-xdeb_*.apk
pkg_basename=v2ray_${PKG_VERSION}-${PKG_RELEASE}-xdeb
if [ ! -e "./dist/$pkg_basename"_x86_64.apk ]; then
    pkg_basename=v2ray_${PKG_VERSION}_p${PKG_RELEASE}-xdeb
fi

# install .apk package (x86_64)
apk --allow-untrusted add ./dist/${pkg_basename}_x86_64.apk
/usr/bin/v2ray version | head -n1 | grep "^V2Ray $PKG_VERSION (V2Fly) "

# uninstall .apk package (x86_64)
test -x /usr/bin/v2ray
apk del v2ray
test ! -x /usr/bin/v2ray

# install .apk package (x86)
apk --allow-untrusted add ./dist/${pkg_basename}_x86.apk
/usr/bin/v2ray version | head -n1 | grep "^V2Ray $PKG_VERSION (V2Fly) "

# uninstall .apk package (x86)
test -x /usr/bin/v2ray
apk del v2ray
test ! -x /usr/bin/v2ray
