#!/bin/bash

if [ "$HOSTNAME" = "${HOSTNAME#runner-}" ] # Don't print HOSTNAME on GitLab Runner.
then
    PS4='\n\[\033[1;94m\]$(date +%FT%T%z) ${HOSTNAME}:${0}:${LINENO} + \e[m\]'
else
    PS4='\n\[\033[1;94m\]$(date +%FT%T%z) ${0}:${LINENO} + \e[m\]'
fi

. /etc/os-release

set -eux


###########################
##                       ##
##  SET UP BUILDING ENV  ##
##                       ##
###########################

case $ID in
debian)
    case $VERSION_CODENAME in
    bullseye|bookworm)
        CODENAME=$VERSION_CODENAME
        ;;
    *)
        echo "ERROR: VERSION_CODENAME not found: $VERSION_CODENAME"
        exit 1
        ;;
    esac
    ;;
alpine)
    CODENAME=alpine
    ;;
*)
    echo "ERROR: ID not found: $ID"
    exit 1
    ;;
esac
export CODENAME

src_dir=$CI_PROJECT_DIR/v2ray-core-${PKG_VERSION}
dist_dir=$CI_PROJECT_DIR/dist/$CODENAME

# Prepare required commands
case $CODENAME in
bullseye|bookworm)
    apt-get update
    command -v envsubst >/dev/null || apt-get install --yes --no-install-recommends gettext
    command -v file >/dev/null || apt-get install --yes file
    command -v wget >/dev/null || apt-get install --yes wget
    ;;
alpine)
    command -v envsubst >/dev/null || apk add gettext
    command -v file >/dev/null || apk add file
    command -v wget >/dev/null || apk add wget
    ;;
*)
    echo "ERROR: case not found: CODENAME='$CODENAME'"
    exit 1
esac

# Prepare required command: nfpm
command -v nfpm >/dev/null || wget --no-verbose -O- https://xdeb.gitlab.io/nfpm/nfpm-static_amd64.tar.gz | tar -C /usr/local/bin -xz
nfpm --version

cd $src_dir


# ###############
# ##           ##
# ##  DO TEST  ##
# ##           ##
# ###############
#
# # FIXME: do a real TEST.
# go fmt $(go list ./... | grep -v /vendor/) || echo "******** TEST FAILED ********"
# go vet $(go list ./... | grep -v /vendor/) || echo "******** TEST FAILED ********"
# go test -race $(go list ./... | grep -v /vendor/) || echo "******** TEST FAILED ********"

################
##            ##
##  DO BUILD  ##
##            ##
################

ldflags="-s -w -buildid=''"
ldflags="$ldflags -X 'github.com/v2fly/v2ray-core/v5.codename=V2Fly'"
ldflags="$ldflags -X 'github.com/v2fly/v2ray-core/v5.build=$(date -u +%FT%TZ)'"

GOARCH=amd64 go build -o $dist_dir/amd64/v2ray -ldflags="$ldflags" -trimpath ./main
GOARCH=386   go build -o $dist_dir/386/v2ray   -ldflags="$ldflags" -trimpath ./main

$dist_dir/amd64/v2ray version
$dist_dir/386/v2ray version

file $dist_dir/amd64/v2ray
file $dist_dir/386/v2ray

case $CODENAME in
bullseye|bookworm)
    ldd $dist_dir/amd64/v2ray
    ! ldd $dist_dir/386/v2ray
    ;;
alpine)
    ! ldd $dist_dir/amd64/v2ray
    ! ldd $dist_dir/386/v2ray
    ;;
*)
    echo "ERROR: case not found: CODENAME='$CODENAME'"
    exit 1
esac

##################
##              ##
##  DO PACKING  ##
##              ##
##################

cd $CI_PROJECT_DIR

# import the sign key
test -n "$PRIVATE_SIGKEY"  # ensure $PRIVATE_SIGKEY is not empty
echo "$PRIVATE_SIGKEY" | base64 -d > key.gpg

mkdir -p dist/noarch
cp -v $src_dir/README.md dist/noarch/README.md
cp -v $src_dir/LICENSE   dist/noarch/LICENSE

case $CODENAME in
bullseye)
    PKG_ARCH=amd64 TARGET_CODENAME=buster envsubst < nfpm.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin
    PKG_ARCH=386   TARGET_CODENAME=buster envsubst < nfpm.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin

    PKG_ARCH=amd64 TARGET_CODENAME=bullseye envsubst < nfpm.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin
    PKG_ARCH=386   TARGET_CODENAME=bullseye envsubst < nfpm.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin

    PKG_ARCH=amd64 TARGET_CODENAME=focal envsubst < nfpm.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin
    PKG_ARCH=386   TARGET_CODENAME=focal envsubst < nfpm.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin
    ;;
bookworm)
    PKG_ARCH=amd64 TARGET_CODENAME=bookworm envsubst < nfpm.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin
    PKG_ARCH=386   TARGET_CODENAME=bookworm envsubst < nfpm.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin

    PKG_ARCH=amd64 TARGET_CODENAME=jammy envsubst < nfpm.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin
    PKG_ARCH=386   TARGET_CODENAME=jammy envsubst < nfpm.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin

    PKG_ARCH=amd64 TARGET_CODENAME=lunar envsubst < nfpm.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin
    PKG_ARCH=386   TARGET_CODENAME=lunar envsubst < nfpm.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin

    PKG_ARCH=amd64 TARGET_CODENAME=mantic envsubst < nfpm.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin
    PKG_ARCH=386   TARGET_CODENAME=mantic envsubst < nfpm.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin
    ;;
alpine)
    PKG_ARCH=amd64 envsubst < nfpm-static.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin
    PKG_ARCH=386   envsubst < nfpm-static.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin

    sed -i 's/${PKG_RELEASE}-xdeb~${TARGET_CODENAME}/${PKG_RELEASE}-xdeb/' nfpm.yaml
    sed -i '/^replaces:/d;/^conflicts:/d;/^  - ${PKG_NAME}-static/d' nfpm.yaml
    PKG_ARCH=amd64 envsubst < nfpm.yaml | nfpm pkg -p apk -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin
    PKG_ARCH=386   envsubst < nfpm.yaml | nfpm pkg -p apk -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin
    ;;
*)
    echo "ERROR: case not found: CODENAME=$CODENAME"
    exit 1
esac
