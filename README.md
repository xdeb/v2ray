# v2ray

A Platform for building proxies to bypass network restrictions.

This repository provides the latest Debian/Ubuntu package.

## Usage (APT installation)

1. Set up the APT key

    Add the repository's APT Key by following command:

        wget -O /etc/apt/trusted.gpg.d/xdeb-gitlab-io.gpg https://xdeb.gitlab.io/v2ray/pubkey.gpg

    or (if you prefer text formatted key file)

        wget -O /etc/apt/trusted.gpg.d/xdeb-gitlab-io.asc https://xdeb.gitlab.io/v2ray/pubkey.asc

2. Set up APT sources

    Create a new source list file at `/etc/apt/sources.list.d/xdeb-gitlab-io.list` with the
    content as following (debian/bullseye for example):

        deb https://xdeb.gitlab.io/v2ray/debian bullseye contrib

    The avaliable source list files are:

    * Debian 10 (buster)

        deb https://xdeb.gitlab.io/v2ray/debian buster contrib

    * Debian 11 (bullseye)

        deb https://xdeb.gitlab.io/v2ray/debian bullseye contrib

    * Debian 12 (bookworm)

        deb https://xdeb.gitlab.io/v2ray/debian bookworm contrib

    * Ubuntu 20.04 TLS (Focal Fossa)

        deb https://xdeb.gitlab.io/v2ray/ubuntu focal universe

    * Ubuntu 22.04 TLS (Jammy Jellyfish)

        deb https://xdeb.gitlab.io/v2ray/ubuntu jammy universe

    * Ubuntu 23.04 (Lunar Lobster)

        deb https://xdeb.gitlab.io/v2ray/ubuntu lunar universe

    * Ubuntu 23.10 (Mantic Minotaur)

        deb https://xdeb.gitlab.io/v2ray/ubuntu mantic universe

3. Install `v2ray`

        apt-get update && apt-get install v2ray

## Usage (download binary)

All binaries are stored at `https://xdeb.gitlab.io/v2ray/v2ray_<OS>_<ARCH>.gz`.

For example, you can download x64 binary for _Debian 12_ at <https://xdeb.gitlab.io/v2ray/v2ray_bookworm_amd64.gz>. You can also download i386 binary for _Ubuntu 22.04_ at <https://xdeb.gitlab.io/v2ray/v2ray_jammy_i386.gz>. Then unzip the downloaded file by `gzip -d` command. Change the file permission by `chmod` command finally.

For instance, download the binary on _Debian 12 (amd64)_, and install it, we can use a one-line command like,

    wget -O- https://xdeb.gitlab.io/v2ray/v2ray_bookworm_amd64.gz | gzip -d > /usr/local/bin/v2ray && chmod 755 /usr/local/bin/v2ray
